HarryPotter Book-Trading example
===================
 
This is a modified version of the bookTrading example from jadeExamples.jar

Usage
---------

Start the startBookTradingFreshCompile.cmd (or double-click)

Explanation
-----------------

* the source files are compiled
* jade.Boot -gui is instanciated
* Three seller agents are generated, that sell HarryPotter for a random price.
* Two buyer agents are launched after each other, that will buy with a x second delay HarryPotter.
* After the 2nd buyer is finished, just close jadeMain window to terminate the seller-agents


Notes
---------
Manual usage:
Compile
```shell
// Compile (optional)
> javac -d classes src/*.java
// Start Main Container
> java -cp lib/jade.jar jade.Boot -gui
// Start seller agents
> java -cp lib/jade.jar;classes jade.Boot -container -agents seller1:examples.bookTrading.BookSellerAgent(HerryPotter)
> java -cp lib/jade.jar;classes jade.Boot -container -agents seller2:examples.bookTrading.BookSellerAgent(HerryPotter)
> java -cp lib/jade.jar;classes jade.Boot -container -agents seller3:examples.bookTrading.BookSellerAgent(HerryPotter)
// Start buyer agents
> java -cp lib/jade.jar;classes jade.Boot -container -agents buyer1:examples.bookTrading.BookBuyerAgent(HerryPotter)
> java -cp lib/jade.jar;classes jade.Boot -container -agents buyer1:examples.bookTrading.BookBuyerAgent(HerryPotter)
```