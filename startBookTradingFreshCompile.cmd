:: BookTrading example
:: ===================
:: 
:: This script will start the BookTrading Example as a jade.Agent example
::  Please make sure that jade.jar and jadeExamples.jar are present in CLASS_PATH global variable
::  Otherwise, you can specify the jar files via: 
::   >> java -cp lib/jade.jar;lib/jadeExapmles.jar jade.Boot ....
:: PS: START lets execute a program in a new seperate window

@ECHO OFF
ECHO This script will compile and start the modified bookTrading example

:: Compile all *.java files in /src
javac -d classes src/*.java
pause

:: start main instance
START "jade-Main" /MIN java -cp lib/jade.jar jade.Boot -gui
ECHO Starting GUI...
pause

:: start some sellers in the background
START "seller1" /MIN java -cp lib/jade.jar;classes jade.Boot -container -agents seller1:examples.bookTrading.BookSellerAgent(HerryPotter)
START "seller2" /MIN java -cp lib/jade.jar;classes jade.Boot -container -agents seller2:examples.bookTrading.BookSellerAgent(HerryPotter)
START "seller3" /MIN java -cp lib/jade.jar;classes jade.Boot -container -agents seller3:examples.bookTrading.BookSellerAgent(HerryPotter)

:: start a buyer
START "buyer1" java -cp lib/jade.jar;classes jade.Boot -container -agents buyer1:examples.bookTrading.BookBuyerAgent(HerryPotter)
ECHO After 10s buyer1 will buy the cheapest book.
pause
ECHO After 10s buyer2 will buy the cheapest book.
START "buyer2" java -cp lib/jade.jar;classes jade.Boot -container -agents buyer1:examples.bookTrading.BookBuyerAgent(HerryPotter)
ECHO Program finished - Please close jade-Main
pause
